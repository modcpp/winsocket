## WinSocket Application

This application exemplifies the usage of Widows Sockets. The solution contains two applications: a Client and a Server.

Code explanations can be found at: https://docs.microsoft.com/en-us/windows/desktop/winsock/getting-started-with-winsock

** TODOS for a Modern Approach: **

- no magic numbers (including version)
- no printf
- error handeled with exceptions
- no NULL
- declare before usage, not in advance
- variable/function names are correct in english ("buf" and "len" are not english words)
- incapsulate code, and use RAII (Resource acquisition is initialization) to be shure that everithing is cleaned up correctly and when no longer needed

*** Optionaly ***

- each deaclaration on a separate line